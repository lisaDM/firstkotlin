package com.example.task1kotlin.viewmodel

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.task1kotlin.R
import com.example.task1kotlin.Util.isNetworkConnected
import com.example.task1kotlin.repository.MainRepository

class LoginViewModel (var mContext: Context) : ViewModel(){
    var email: String = ""
    var password: String = ""
    var message: String = ""
    private val logInResult = MutableLiveData<String>()
    var loginRespository = MainRepository()
    var liveProgressBar = MutableLiveData<Boolean>()


    fun getLogInResult(): MutableLiveData<String> {
        return loginRespository.logInResult
    }


    fun refresh() {

    }

    fun login(email: String, password: String) {
        if (isNetworkConnected(mContext)) {
            liveProgressBar.value = true
           loginRespository.fetchdata(email, password)

        } else {
            logInResult.value = mContext.resources.getString(R.string.no_internet_connection)
        }
    }




    fun performValidation() {

        if (email.isBlank()) {
            logInResult.value = "Invalid username"
            return
        }

        if (password.isBlank()) {
            logInResult.value = "Invalid password"
            return
        }


        login(email,password)
    }


}