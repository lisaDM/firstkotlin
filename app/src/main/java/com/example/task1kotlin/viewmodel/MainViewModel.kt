package com.example.androidbasicmvvm.mvvm.viewmodel

import android.content.Context
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.task1kotlin.model.Employee
import com.example.task1kotlin.model.EmployeeData
import com.example.task1kotlin.network.EmployeeApi
import com.example.task1kotlin.network.EmployeeService
import com.google.gson.Gson
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainViewModel(var mContext: Context) : ViewModel() {
    var employeeDataList = ArrayList<EmployeeData>()
    var employeeDataListdata = MutableLiveData<List<EmployeeData>>()
    private val  disposable = CompositeDisposable()
    var employeeLoadError = MutableLiveData<Boolean>()
    val loading  = MutableLiveData<Boolean>();
    fun refresh(){
        fetchEmployee();
    }

    private  fun fetchEmployee(){
       loading.value =true;
        val apiService = EmployeeService.client.create(EmployeeApi::class.java)
        val call = apiService.getEmployees()


        call.enqueue(object : Callback<Employee> {
            override fun onResponse(call: Call<Employee>, response: Response<Employee>) {
                if (response.code() == 200) {
                    val employees__ = response.body()
                    if (employees__ != null) {
                        if (employees__.particulars.equals("success")){
                            employeeDataList = employees__.data!!
                            employeeDataListdata.value = employeeDataList
                            loading.value = false
                        }
                        else{

                        }
                    }
                }
            }
            override fun onFailure(call: Call<Employee>, t: Throwable) {
               print(t.message)

            }
        })


/*
        disposable.add(
            employeeService.getEmployee()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableSingleObserver<Employee>(){

                    override fun onError(e: Throwable) {
                        employeeLoadError .value = true
                        loading.value = false
                    }

                    override fun onSuccess(t: Employee) {

                        print(t.toString())
                        if (t.particulars.equals("success")){
                            EmployeeList.value = t.data
                        }

                    }

                })

        )*/


    }


    override fun onCleared() {
        super.onCleared()
        disposable.clear()
    }
}