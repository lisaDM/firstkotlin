package com.example.task1kotlin.viewmodel

import android.content.Context
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.task1kotlin.R
import com.example.task1kotlin.Util.Utills
import com.example.task1kotlin.Util.isNetworkConnected
import com.example.task1kotlin.model.RadioButtonList
import com.example.task1kotlin.model.SignupRespose
import com.example.task1kotlin.model.UserDetails
import com.example.task1kotlin.network.EmployeeApi
import com.example.task1kotlin.network.EmployeeService
import com.example.task1kotlin.repository.MainRepository
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SignupViewModel(var mContext: Context) : ViewModel() {
    var username: String = ""
    var email: String = ""
    var password: String = ""
    private val logInResult = MutableLiveData<String>()
    var signupRespository = MainRepository()
    var liveProgressBar = MutableLiveData<Boolean>()

    fun getSignup(): MutableLiveData<String> {
        return signupRespository.signupResult
    }



    fun performValidation() {

        if (username.isBlank()) {
            logInResult.value = "Invalid username"
            return
        }

        if (password.isBlank()) {
            logInResult.value = "Invalid password"
            return
        }
        if (email.isBlank()) {
            logInResult.value = "Invalid email"
            return
        }
        signup(username,email,password)
    }

    private fun signup(username : String,email: String, password: String) {

        if (isNetworkConnected(mContext)) {
            liveProgressBar.value = true
            signupRespository.fetchSignup(username,email, password)

        } else {
            logInResult.value = mContext.resources.getString(R.string.no_internet_connection)
        }

    }



}