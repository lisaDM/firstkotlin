package com.example.task1kotlin.viewmodel

import android.content.Context
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.task1kotlin.Util.Utills
import com.example.task1kotlin.model.RadioButtonData
import com.example.task1kotlin.model.RadioButtonList
import com.example.task1kotlin.network.EmployeeApi
import com.example.task1kotlin.network.EmployeeService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AddonViewModel(var mContext: Context) : ViewModel() {

    var addonArrayList = ArrayList<RadioButtonData>()

    var AddonDataListdata = MutableLiveData<List<RadioButtonData>>()
    var employeeLoadError = MutableLiveData<Boolean>()
    val loading = MutableLiveData<Boolean>();
    fun refresh() {
        fetchdata();
    }

    private fun fetchdata() {

        val categoryId = 14

        val apiService = EmployeeService.client.create(EmployeeApi::class.java)
        val requestCall = apiService.getService("user",  Utills.data,"default",categoryId)
        if (requestCall != null) {
            requestCall.enqueue(object : Callback<RadioButtonList?> {
                override fun onResponse(
                    call: Call<RadioButtonList?>, response: Response<RadioButtonList?>) {
                    val responsebody = response.body()
                    if (responsebody != null) {
                        if (responsebody.particulars.equals("false")){
                            print(responsebody.toString())
                            addonArrayList = responsebody.data!!
                            AddonDataListdata.value = addonArrayList
                            loading.value = false
                          //  Toast.makeText(this,"data")
                        } else{

                        }
                    }


                }

                override fun onFailure(call: Call<RadioButtonList?>, t: Throwable) {
                    print(t.message.toString())
                }
            })
        }
    }
}





