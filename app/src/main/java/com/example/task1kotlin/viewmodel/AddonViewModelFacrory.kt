package com.example.task1kotlin.viewmodel

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.androidbasicmvvm.mvvm.viewmodel.MainViewModel

class AddonViewModelFacrory(var mContext : Context) : ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return AddonViewModel(mContext) as T
    }
}