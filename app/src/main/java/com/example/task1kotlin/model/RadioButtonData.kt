package com.example.task1kotlin.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.ArrayList

class RadioButtonData {

    @SerializedName("id")
    @Expose
    var id:  String? = ""

    @SerializedName("title")
    @Expose
    var title:  String? = ""

    @SerializedName("type")
    @Expose
    var type:  String? = ""

    @SerializedName("data")
    @Expose
    var data: ArrayList<DetailsData>? = null

    @SerializedName("profile_image")
    @Expose
    var profileImage:  String? = ""

}