package com.example.task1kotlin.model

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class SignupRespose {

    @SerializedName("error")
    @Expose
    private var error: Boolean? = null

    @SerializedName("statusCode")
    @Expose
    private var statusCode: Int? = null

    @SerializedName("message")
    @Expose
    private var message: String? = null

    @SerializedName("userDetails")
    @Expose
    private var userDetails: UserDetails? = null

    @SerializedName("accessToken")
    @Expose
    private var accessToken: String? = null

    fun getError(): Boolean? {
        return error
    }

    fun setError(error: Boolean?) {
        this.error = error
    }

    fun getStatusCode(): Int? {
        return statusCode
    }

    fun setStatusCode(statusCode: Int?) {
        this.statusCode = statusCode
    }

    fun getMessage(): String? {
        return message
    }

    fun setMessage(message: String?) {
        this.message = message
    }

    fun getUserDetails(): UserDetails? {
        return userDetails
    }

    fun setUserDetails(userDetails: UserDetails?) {
        this.userDetails = userDetails
    }

    fun getAccessToken(): String? {
        return accessToken
    }

    fun setAccessToken(accessToken: String?) {
        this.accessToken = accessToken
    }
}