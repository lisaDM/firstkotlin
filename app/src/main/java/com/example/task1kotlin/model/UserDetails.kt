package com.example.task1kotlin.model

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class UserDetails {

    @SerializedName("_id")
    @Expose
    private var id: String? = null

    fun getId(): String? {
        return id
    }

    fun setId(id: String?) {
        this.id = id
    }
}