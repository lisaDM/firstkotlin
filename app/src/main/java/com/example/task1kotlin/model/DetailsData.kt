package com.example.task1kotlin.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class DetailsData  : Serializable{

    @SerializedName("id")
    @Expose
    var id:  String? = ""

    @SerializedName("name")
    @Expose
    var name:  String? = ""

    @SerializedName("price")
    @Expose
    var price:  String? = ""

    @SerializedName("currencyType")
    @Expose
    var currencyType:  String? = ""

    @SerializedName("titleId")
    @Expose
    var titleId:  String? = ""
}