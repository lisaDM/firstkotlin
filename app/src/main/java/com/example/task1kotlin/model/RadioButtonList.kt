package com.example.task1kotlin.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.ArrayList

class RadioButtonList {

    @SerializedName("data")
    @Expose
    var data: ArrayList<RadioButtonData>? = null

    @SerializedName("error")
    @Expose
    var particulars:  String? = ""
}