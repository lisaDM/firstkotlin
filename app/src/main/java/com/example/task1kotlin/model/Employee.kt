package com.example.task1kotlin.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.ArrayList

class Employee {


    @SerializedName("error")
    @Expose
    var particulars: String? = ""

    @SerializedName("msg")
    @Expose
    var msg: String? = ""

    @SerializedName("data")
    @Expose
    var data: ArrayList<EmployeeData>? = null
}