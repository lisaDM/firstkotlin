package com.example.task1kotlin.interfac

interface LogInHandler {

    fun onLogInClicked()
}