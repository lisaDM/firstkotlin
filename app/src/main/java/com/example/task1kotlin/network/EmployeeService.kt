package com.example.task1kotlin.network

import com.example.task1kotlin.model.Employee
import io.reactivex.Single
import io.reactivex.plugins.RxJavaPlugins
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

object EmployeeService {
   // private val BASE_URL = "http://dummy.restapiexample.comx";
    //private val BASE_URL = "http://134.209.147.250:3000";
    private val BASE_URL = "http://139.59.43.169:3006/";
    //http://139.59.43.169:3006/

    private var retrofit: Retrofit? = null

    val client: Retrofit
        get() {
            if (retrofit == null) {
                retrofit = Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
            }
            return retrofit!!
        }


  //  private val api: EmployeeApi

  /*  init {
        api = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
            .create(EmployeeApi::class.java)
    }

    fun getEmployee(): Single<Employee> {
        return api.getEmployees()
    }*/

}