package com.example.task1kotlin.network

import com.example.task1kotlin.model.Employee
import com.example.task1kotlin.model.LoginResponse
import com.example.task1kotlin.model.RadioButtonList
import com.example.task1kotlin.model.SignupRespose
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*


interface EmployeeApi {
    //http://dummy.restapiexample.com
    @GET("/api/v1/employees")
    fun getEmployees(): Call<Employee>

    @Headers(
        "Content-Type: application/x-www-form-urlencoded"
    )
    @POST("api/users/serviceAddons")
    @FormUrlEncoded
    fun getService(
        @Header("Role") role: String,
        @Header("access_token") accesToken: String,
        @Header("lang") lang: String,
        @Field("categoryId") categoryId: Int?
    ): Call<RadioButtonList?>?

    @Headers("Content-Type: application/x-www-form-urlencoded")
    @POST("register")
    @FormUrlEncoded
    fun register(
        @Field("email") email: String, @Field("userName") userName: String
        , @Field("password") password: String
    ): Call<SignupRespose>


    @Headers("Content-Type: application/x-www-form-urlencoded")
    @POST("login")
    @FormUrlEncoded
    fun login(
        @Field("email") email: String, @Field("password") password: String
    ): Call<LoginResponse>

}//  @Body categoryId: Int?