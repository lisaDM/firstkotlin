package com.example.task1kotlin.ui

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.task1kotlin.R
import com.example.task1kotlin.databinding.ActivityRadioButtonBinding
import com.example.task1kotlin.model.DetailsData
import com.example.task1kotlin.viewmodel.AddonViewModel
import com.example.task1kotlin.viewmodel.AddonViewModelFacrory
import kotlinx.android.synthetic.main.activity_radio_button.*


class AddonsActivity : AppCompatActivity() , AddonsCheckboxAdapter.SelectedOption {
    lateinit var mainBinding: ActivityRadioButtonBinding
    lateinit var radioVM: AddonViewModel
    lateinit var addonsCheckboxAdapter: AddonsCheckboxAdapter
    lateinit var adapter: AddonRadioButtonAdapter
     var  addonArrayList = ArrayList<DetailsData>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mainBinding = DataBindingUtil.setContentView(this, R.layout.activity_radio_button)
        mainBinding.lifecycleOwner = this

        initBasics()
        setAdapter()
        observiewModel()
    }

    private fun setAdapter() {
        addonsCheckboxAdapter = AddonsCheckboxAdapter(this, radioVM.addonArrayList, this)
        rv_rediobutton.adapter = addonsCheckboxAdapter
    }


    fun initBasics() {
        radioVM = ViewModelProvider(this, AddonViewModelFacrory(this))[AddonViewModel::class.java]
        mainBinding.radioVM = this
        radioVM.refresh()
    }

    fun observiewModel() {
        radioVM.AddonDataListdata.observe(this, Observer { employee ->
            employee?.let { addonsCheckboxAdapter.updateList(it) }
        })
        radioVM.employeeLoadError.observe(this, Observer { isError ->
            isError?.let { Toast.makeText(this, "errot", Toast.LENGTH_SHORT).show() }
        })
        radioVM.loading.observe(this, Observer { isLoading -> })
    }

    override fun selectedOption(detailsData: java.util.ArrayList<DetailsData>?) {


       /* val intent = Intent(this, AddonSecondActivity::class.java)
       intent.putExtra("data",detailsData)
        // start your next activity
        startActivity(intent)*/

        val androidFragment: Fragment = LoadDataFragment(detailsData,this)
        replaceFragment(androidFragment)
        rv_rediobutton.visibility=  View.GONE
        toolbar.visibility=  View.GONE
        tv_title_activity.visibility=  View.GONE
        //val fragment = Fragment()


      /*  val fm: FragmentManager = supportFragmentManager
        val ft: FragmentTransaction = fm.beginTransaction()
        ft.replace(R.id.frameContainer, fragment)
        ft.commit()*/
        }

    private fun replaceFragment(androidFragment: Fragment) {

        val fragmentManager = this.supportFragmentManager
        val fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.frameContainer, androidFragment)
        fragmentTransaction.commit()

    }


}

