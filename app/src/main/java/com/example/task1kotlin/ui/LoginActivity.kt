package com.example.task1kotlin.ui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.task1kotlin.R
import com.example.task1kotlin.databinding.ActivityLoginBinding
import com.example.task1kotlin.databinding.ActivitySignupBinding
import com.example.task1kotlin.interfac.LogInHandler
import com.example.task1kotlin.viewmodel.LoginViewModel
import com.example.task1kotlin.viewmodel.LoginViewModelFactory
import com.example.task1kotlin.viewmodel.SignupViewModel
import com.example.task1kotlin.viewmodel.SignupViewModelFactory
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_login.et_email
import kotlinx.android.synthetic.main.activity_login.et_password
import kotlinx.android.synthetic.main.activity_signup.*

class LoginActivity : AppCompatActivity() , LogInHandler {
    lateinit var mainBinding: ActivityLoginBinding
    lateinit var signinViewmodel: LoginViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mainBinding = DataBindingUtil.setContentView(this, R.layout.activity_login)
        mainBinding.lifecycleOwner = this
        initBasics()
    }

    private fun initBasics() {
        signinViewmodel = ViewModelProvider(this, LoginViewModelFactory(this))[LoginViewModel::class.java]

        mainBinding.signinVm = signinViewmodel
        mainBinding.handlr = this
        signinViewmodel.refresh()

        tv_text_heading.setOnClickListener {
            intent = Intent(this, SignupActivity::class.java)
            startActivity(intent)
        }

        signinViewmodel.getLogInResult().observe(this, Observer { result ->
            Toast.makeText(this, result, Toast.LENGTH_SHORT).show()
        })
    }

    override fun onLogInClicked() {
        signinViewmodel.performValidation()

    }
}
