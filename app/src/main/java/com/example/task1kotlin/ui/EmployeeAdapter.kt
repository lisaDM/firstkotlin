package com.example.task1kotlin.ui

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.task1kotlin.R
import com.example.task1kotlin.model.Employee
import com.example.task1kotlin.model.EmployeeData
import kotlinx.android.synthetic.main.item_employee_details.view.*

class EmployeeAdapter(
    private val employeeList: ArrayList<EmployeeData>, var mContext: Context): RecyclerView.Adapter<EmployeeAdapter.EmployeeListVH>(){
    override fun getItemCount() = employeeList.size
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EmployeeListVH {
        var view = LayoutInflater.from(mContext).inflate(R.layout.item_employee_details, parent, false);
        return EmployeeListVH(view)
    }

    override fun onBindViewHolder(holder:EmployeeListVH, position: Int) {

        val employeeInfo = employeeList[holder.adapterPosition]
        holder.itemView.emp_name.text = employeeInfo.employeeName
    }

    fun updateEmployee(it: List<EmployeeData>) {
        employeeList.clear()
        employeeList.addAll(it)
        notifyDataSetChanged()

    }

    class EmployeeListVH(view: View) : RecyclerView.ViewHolder(view) {
        val emplyeName = view.emp_name;
        fun bindData(emplyeedata :EmployeeData) {
           // emplyeName.text= emplyeedata.data.get(position).employee_name
        }
    }
}