package com.example.task1kotlin.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.example.task1kotlin.R
import com.example.task1kotlin.databinding.ActivityAddonSecondBinding
import com.example.task1kotlin.databinding.ActivityRadioButtonBinding
import com.example.task1kotlin.model.DetailsData
import com.example.task1kotlin.viewmodel.AddonSeconViewModel
import com.example.task1kotlin.viewmodel.AddonViewModel
import com.example.task1kotlin.viewmodel.AddonViewModelFacrory
import com.example.task1kotlin.viewmodel.AddonViewModelFactorySecondActivity
import kotlinx.android.synthetic.main.activity_addon_second.*
import kotlinx.android.synthetic.main.activity_radio_button.*

class AddonSecondActivity : AppCompatActivity() {
    lateinit var mainBinding: ActivityAddonSecondBinding
    var addonArrayList = ArrayList<DetailsData>()
    lateinit var addonActivity: AddonViewModelFactorySecondActivity
    lateinit var viewModel: AddonSeconViewModel
    lateinit var adapter: AddonRadioButtonAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_addon_second)
        mainBinding = DataBindingUtil.setContentView(this, R.layout.activity_addon_second)
        mainBinding.lifecycleOwner = this
        addonArrayList = getIntent().getSerializableExtra("data") as ArrayList<DetailsData>

        print(addonArrayList)
        initBasics()
        setAdapter()
    }

    private fun setAdapter() {
        adapter = AddonRadioButtonAdapter(this, addonArrayList)
        rv_check_button.adapter = adapter
    }

    fun initBasics() {
        viewModel = ViewModelProvider(
            this,
            AddonViewModelFactorySecondActivity(this)
        )[AddonSeconViewModel::class.java]
        mainBinding.addonActivity = this
        viewModel.refresh()
    }

}
