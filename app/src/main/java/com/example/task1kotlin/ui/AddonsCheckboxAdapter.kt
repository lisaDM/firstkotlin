package com.example.task1kotlin.ui

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.task1kotlin.R
import com.example.task1kotlin.model.DetailsData
import com.example.task1kotlin.model.RadioButtonData
import kotlinx.android.synthetic.main.cell_radio_button.view.*

class AddonsCheckboxAdapter(var mContext: Context, val radiobuttonList: ArrayList<RadioButtonData>, val listener: SelectedOption) :


    RecyclerView.Adapter<AddonsCheckboxAdapter.RadioButtonViewHolder>() {

    var detailsData: ArrayList<DetailsData> = ArrayList()

    class RadioButtonViewHolder(view: View) : RecyclerView.ViewHolder(view) {


    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RadioButtonViewHolder {

        var view = LayoutInflater.from(mContext).inflate(R.layout.cell_radio_button, parent, false)
        return RadioButtonViewHolder(view)

    }

    override fun getItemCount(): Int {
        return radiobuttonList.size
    }

    fun updateList(it: List<RadioButtonData>) {
        radiobuttonList.clear()
        radiobuttonList.addAll(it)
        notifyDataSetChanged()

    }

    override fun onBindViewHolder(holder: RadioButtonViewHolder, position: Int) {
        val details = radiobuttonList[holder.adapterPosition]
        holder.itemView.tv_add.text = details.title
        print(details.data)
        holder.itemView.setOnClickListener {
            listener.selectedOption(details.data)
        }


    }

    interface SelectedOption {
        fun selectedOption(detailsData: java.util.ArrayList<DetailsData>?)
    }


}


