package com.example.task1kotlin.ui

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.task1kotlin.R
import com.example.task1kotlin.model.DetailsData
import com.example.task1kotlin.model.EmployeeData
import com.example.task1kotlin.model.RadioButtonData
import kotlinx.android.synthetic.main.cell_details.view.*
import kotlinx.android.synthetic.main.cell_radio_button.view.*
import kotlinx.android.synthetic.main.item_employee_details.view.*

class AddonRadioButtonAdapter(
    var mContext: Context,
    private val checkBoxArraylist: ArrayList<DetailsData>
) :
    RecyclerView.Adapter<AddonRadioButtonAdapter.AddonRadioButtonViewHolder>() {

    class AddonRadioButtonViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AddonRadioButtonViewHolder {
        var view = LayoutInflater.from(mContext).inflate(R.layout.cell_details, parent, false);
        return AddonRadioButtonViewHolder(view)
    }

    override fun getItemCount(): Int {
        return checkBoxArraylist.size
    }

    override fun onBindViewHolder(holder: AddonRadioButtonViewHolder, position: Int) {

        val details = checkBoxArraylist[holder.adapterPosition]
        holder.itemView.tv_add_name.text = details.name
        // holder.itemView.tv_add_name = details.name

    }

    /* fun updateList(it: List<checkBoxArraylist>) {
         checkBoxArraylist.clear()
         checkBoxArraylist.addAll(it)
         notifyDataSetChanged()

     }*/
}