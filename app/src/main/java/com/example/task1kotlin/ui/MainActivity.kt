package com.example.task1kotlin.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.RecyclerView
import com.example.androidbasicmvvm.mvvm.viewmodel.MainVMFactory
import com.example.androidbasicmvvm.mvvm.viewmodel.MainViewModel
import com.example.task1kotlin.R
import com.example.task1kotlin.databinding.ActivityMainBinding
import com.example.task1kotlin.model.Employee
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {
    lateinit var mainBinding: ActivityMainBinding
    lateinit var mainVM: MainViewModel
    lateinit var itemEmployeeListAdapter: EmployeeAdapter
    private var value2: Int = 0;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        mainBinding.lifecycleOwner = this

        initBasics()
        setAdapter()
        observiewModel()
    }

    fun initBasics() {
        mainVM = ViewModelProvider(this, MainVMFactory(this))[MainViewModel::class.java]
        mainBinding.mainVM = this
        mainVM.refresh()
    }

    fun setAdapter() {
        itemEmployeeListAdapter = EmployeeAdapter(mainVM.employeeDataList, this)
        rv_recylerview.adapter = itemEmployeeListAdapter
    }

    private fun observiewModel() {
        mainVM.employeeDataListdata.observe(this, Observer { employee ->
            employee?.let {
                itemEmployeeListAdapter.updateEmployee(it)
            }
        })
        mainVM.employeeLoadError.observe(this, Observer { isError ->
            isError?.let { Toast.makeText(this, "errot", Toast.LENGTH_SHORT).show() }
        })
        mainVM.loading.observe(this, Observer { isLoading -> })

        /* mainVM.employeeDataList.obse(this, Observer { employee ->
             employee?.let {
                 itemEmployeeListAdapter.updateEmployee(it) }
         })
         mainVM.employeeLoadError.observe(this, Observer { isError->
             isError?.let { Toast.makeText(this,"errot",Toast.LENGTH_SHORT).show()}
         })
         mainVM.loading.observe(this, Observer { isLoading -> })*/
    }


}
