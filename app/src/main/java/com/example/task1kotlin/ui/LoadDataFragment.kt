package com.example.task1kotlin.ui

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.task1kotlin.R
import com.example.task1kotlin.model.DetailsData
import java.util.ArrayList

class LoadDataFragment(
    detailsData: ArrayList<DetailsData>?,
    context: Context
) : Fragment() {
    lateinit var adapter: AddonRadioButtonAdapter
    lateinit var fragment_recylerview: RecyclerView
    var detailsDatas: ArrayList<DetailsData>
    lateinit var mContext: Context

    init {
        detailsDatas = detailsData!!
        mContext = context

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.first_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        fragment_recylerview = view.findViewById(R.id.fragment_recylerview)
        fragment_recylerview.layoutManager = LinearLayoutManager(context)
        fragment_recylerview.adapter =AddonRadioButtonAdapter(mContext, detailsDatas)

       /* adapter = AddonRadioButtonAdapter(mContext, detailsDatas)
        fragment_recylerview.adapter*/


    }
}