package com.example.task1kotlin.ui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.EditText
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.task1kotlin.R
import com.example.task1kotlin.databinding.ActivitySignupBinding
import com.example.task1kotlin.interfac.LogInHandler
import com.example.task1kotlin.viewmodel.AddonViewModel
import com.example.task1kotlin.viewmodel.AddonViewModelFacrory
import com.example.task1kotlin.viewmodel.SignupViewModel
import com.example.task1kotlin.viewmodel.SignupViewModelFactory
import kotlinx.android.synthetic.main.activity_signup.*

class SignupActivity : AppCompatActivity(), LogInHandler {
    lateinit var mainBinding: ActivitySignupBinding
    lateinit var signupViewmodel: SignupViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mainBinding = DataBindingUtil.setContentView(this, R.layout.activity_signup)
        mainBinding.lifecycleOwner = this
        Log.d("email::", "" + mainBinding.etEmail.toString())
        initBasics()
    }

    private fun initBasics() {
        signupViewmodel =
            ViewModelProvider(this, SignupViewModelFactory(this))[SignupViewModel::class.java]
        mainBinding.signupVm = signupViewmodel
        mainBinding.handlr = this
        tv_text_login.setOnClickListener {
            intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
        }
        signupViewmodel.getSignup().observe(this, Observer { result ->
            Toast.makeText(this, result, Toast.LENGTH_SHORT).show()
        })

    }

    override fun onLogInClicked() {
        signupViewmodel.performValidation()
    }
}
