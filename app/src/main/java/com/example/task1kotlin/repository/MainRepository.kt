package com.example.task1kotlin.repository

import androidx.lifecycle.MutableLiveData
import com.example.task1kotlin.model.LoginResponse
import com.example.task1kotlin.model.SignupRespose
import com.example.task1kotlin.network.EmployeeApi
import com.example.task1kotlin.network.EmployeeService
import com.example.task1kotlin.viewmodel.LoginViewModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainRepository {

    val loginResponse = MutableLiveData<LoginResponse>()

    val logInResult = MutableLiveData<String>()
    val signupResult = MutableLiveData<String>()

    fun fetchdata(email: String, password: String) {

        val apiService = EmployeeService.client.create(EmployeeApi::class.java)
        val requestCall = apiService.login(email, password)
        requestCall.enqueue(object : Callback<LoginResponse?> {
            override fun onResponse(
                call: Call<LoginResponse?>, response: Response<LoginResponse?>
            ) {
                val responsebody = response.body()
                if (response.isSuccessful) {
                    if (response.code() == 200 && response.body() != null) {
                        if (responsebody != null) {
                            if (responsebody.getError() == false) {
                                print(responsebody.getMessage())
                                logInResult.value = responsebody.getMessage()
                                print(logInResult.value)

                            } else {
                                print(responsebody.getMessage())
                                logInResult.value = responsebody.getMessage()

                            }

                        }


                    }
                }

            }


            override fun onFailure(call: Call<LoginResponse?>, t: Throwable) {
                print(t.message.toString())
            }
        })

    }

    fun fetchSignup(email: String, username: String, password: String) {

        val apiService = EmployeeService.client.create(EmployeeApi::class.java)
        val requestCall = apiService.register(email, username, password)
        if (requestCall != null) {
            requestCall.enqueue(object : Callback<SignupRespose?> {
                override fun onResponse(
                    call: Call<SignupRespose?>, response: Response<SignupRespose?>
                ) {
                    val responsebody = response.body()


                    if (response.isSuccessful) {
                        if (response.code() == 200) {
                            if (responsebody != null) {
                                if (responsebody.getError() == false) {
                                    print(responsebody.getMessage())
                                    signupResult.value = responsebody.getMessage()
                                    print(logInResult.value)

                                } else {
                                    print(responsebody.getMessage())
                                    signupResult.value = responsebody.getMessage()

                                }
                            }

                        }
                    }

                }


                //}

                override fun onFailure(call: Call<SignupRespose?>, t: Throwable) {
                    print(t.message.toString())
                }
            })
        }

    }


}